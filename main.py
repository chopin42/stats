# Import maths modules
from math import sqrt
import statistics
from collections import Counter

# Create a list
data = []

# Get the first value
value = float(input("First value= "))
data.append(value)

# Get the data
while value != '':
    value = input("Next value= ")
    if value != '':
        value = float(value)
        data.append(value)
else:
    print(data)

# Get the average
average = statistics.mean(data)

# Creating the output
out = 0
i = len(data) - 1
while i != -1:
    value = (data[i]-average)*(data[i]-average)
    out = out + value
    i = i - 1

variance = out / len(data)
StdDev = sqrt(variance)

# Getting the scope of data
max = max(data)
min = min(data)
scope = max - min

# Find the median
median = statistics.median(data)

# Find the mode
try:
    mode = statistics.mode(data)
except:
    mode = "null"


# Print the output
print("The average is: ")
print(average)

print("The scope is: ")
print(scope)

print("The median is: ")
print(median)

if mode == 'null':
    print("There is no mode")
else:
    print("The mode is: ")
    print(mode)

print("The variance is: ")
print(variance)

print("The Standard deviation is: ")
print(StdDev)
