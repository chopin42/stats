# Basic Stats tool
Get the basic information from data.

![Screenshot of the result here](finished.png)

This app makes you able to extract the statistics information out of a small data table. In fact, there is no limitation to the data table but writing except your patience, I recommend you to use this app for data table of less than 50 values.

This app has some "cheats" anyway, in deed some simple things such as mode or other things are used using the "statistic" modules. I didn't used this module to compute the variance and the standard deviation, so you can see how to compute these yourself inside the code.

In a further release you will be able to get more educational information on how does it works. Stay tuned!

## Requirements
You need few requirements to build it from source:

* Git (to download it)
* Python3.7

## Build from source
To build it from source you just need to run the following commands:

```
git clone https://gitea.com/chopin42/stats
cd stats
python3.7 main.py
```

### Troubleshooting

If your issue is not one of these search into the [issues](https://gitea/USER/PROJECT/issues) to find yours. If not exists create one and wait for answer by the community and developers. If this issue is caused by a bug it will be solved in the next release. Thanks for contributing to this project!

## Contribute!

You want to contribute? Thanks! You can get all the information at [CONTRIBUTING guide](./CONTRIBUTING.md)

## Meta
This project has been created by SnowCode, thanks to all contributors to make this possible!

This project is under GNU GENERAL PUBLIC LICENSE, to get more information go to [LICENSE file](./LICENSE)
